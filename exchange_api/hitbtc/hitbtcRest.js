const https = require('https');
const queryString = require('querystring');

var HitBTCClient = function(APIKey, APISecret) {
    this.APIKey = APIKey;
    this.APISecret = APISecret;
    this.APIVersion = '2';
    this.APIType = 'live';
};

HitBTCClient.HOSTS = {
    live: 'api.hitbtc.com',
    sandbox: 'demo-api.hitbtc.com'
};
//(ticker, 'public',  {}, callback);
HitBTCClient.prototype._get = function(destination, params, account, callback) {
    let options = {
        host: HitBTCClient.HOSTS[this.APIType],
        path: '/api/' + this.APIVersion + '/' + destination,
        method: 'get',
        headers: {
            'User-Agent': 'Mozilla/4.0 (compatible; HitBTC node.js client)',
            'Content-Type': 'application/json'
        }
    };

    if (!account.includes('public')) {
        this._authorize(options);
    }

    if (Object.keys(params).length) {
        options.path = options.path + '?' + queryString.stringify(params);
    }

    let req = https.request(options, res => {
        res.setEncoding('utf8');
        let buffer = '';
        res.on('data', data => {
            buffer += data;
        });
        let json;
        res.on('end', () => {
            try {
                json = JSON.parse(buffer);
            } catch (err) {
                return callback(err);
            }
            callback(null, json);
        });
    });

    req.on('error', err => {
        callback(err);
    });

    req.on('socket', socket => {
        socket.setTimeout(10000);
        socket.on('timeout', () => {
            req.abort();
        });
    });

    req.end();
};

HitBTCClient.prototype._post = function(destination, params, callback) {
    let options = {
        host: HitBTCClient.HOSTS[this.APIType],
        path: '/api/' + this.APIVersion + '/' + destination,
        method: 'post',
        headers: {
            'User-Agent': 'Mozilla/4.0 (compatible; HitBTC node.js client)',
            'Content-Type': 'application/json'
        }
    };
    this._authorize(options);

    if (Object.keys(params).length) {
        options.path = options.path + '?' + queryString.stringify(params);
    }

    let req = https.request(options, res => {
        res.setEncoding('utf8');
        let buffer = '';
        res.on('data', data => {
            buffer += data;
        });
        let json;
        res.on('end', () => {
            try {
                json = JSON.parse(buffer);
            } catch (err) {
                return callback(err);
            }
            callback(null, json);
        });
    });

    req.on('error', err => {
        callback(err);
    });

    req.on('socket', socket => {
        //socket.setTimeout(5000);
        socket.on('timeout', () => {
            req.abort();
        });
    });

    req.end();
};

HitBTCClient.prototype._delete = function(destination, params, callback) {
    let options = {
        host: HitBTCClient.HOSTS[this.APIType],
        path: '/api/' + this.APIVersion + '/' + destination,
        method: 'delete',
        headers: {
            'User-Agent': 'Mozilla/4.0 (compatible; HitBTC node.js client)',
            'Content-Type': 'application/json'
        }
    };

    this._authorize(options);

    let req = https.request(options, res => {
        res.setEncoding('utf8');
        let buffer = '';
        res.on('data', data => {
            buffer += data;
        });
        let json;
        res.on('end', () => {
            try {
                json = JSON.parse(buffer);
            } catch (err) {
                return callback(err);
            }
            callback(null, json);
        });
    });

    req.on('error', err => {
        callback(err);
    });

    req.on('socket', socket => {
        //socket.setTimeout(5000);
        socket.on('timeout', () => {
            req.abort();
        });
    });
    req.end();
};

HitBTCClient.prototype._authorize = function(options) {
    options.headers = {
        Authorization: 'Basic ' + new Buffer(this.APIKey + ':' + this.APISecret, 'utf8').toString('base64'),
        'User-Agent': 'Mozilla/4.0 (compatible; HitBTC node.js client)',
        'Content-Type': 'application/json'
    };
};

/*
 * Public API Methods
 */
HitBTCClient.prototype.tickerPair = function(pair, callback) {
    this._get('public/ticker/' + pair, {}, 'public', callback);
};

HitBTCClient.prototype.tickerAll = function(callback) {
    this._get('public/ticker', {}, 'public', callback);
};

HitBTCClient.prototype.symbols = function(callback) {
    this._get('public/symbol', {}, 'public', callback);
};

HitBTCClient.prototype.candles = function(symbol, period, limit, callback) {
    this._get('public/candles/' + symbol + '?period=' + period + '&limit=' + limit, {}, 'public', callback);
};

HitBTCClient.prototype.candlesWithParameters = function(symbol, period, limit, callback) {
    let options = {
        host: HitBTCClient.HOSTS[this.APIType],
        path: '/api/' + this.APIVersion + '/' + 'public/candles/' + symbol + '?period=' + period + '&limit=' + limit,
        method: 'get',
        headers: {
            'User-Agent': 'Mozilla/4.0 (compatible; HitBTC node.js client)',
            'Content-Type': 'application/json'
        }
    };

    let req = https.request(options, res => {
        res.setEncoding('utf8');
        let buffer = '';
        res.on('data', data => {
            buffer += data;
        });
        let json;
        res.on('end', () => {
            try {
                json = JSON.parse(buffer);
            } catch (err) {
                return callback(err);
            }
            if (json != null && json != undefined) {
                json['symbol'] = symbol;
                json['period'] = period;
            }
            callback(null, json);
        });
    });

    req.on('error', err => {
        callback(err);
    });

    req.on('socket', socket => {
        socket.setTimeout(10000);
        socket.on('timeout', () => {
            req.abort();
        });
    });

    req.end();
};
/*
 * Trading API Methods
 */

HitBTCClient.prototype.activeOrders = function(callback) {
    this._get('order', {}, 'private', callback);
};

HitBTCClient.prototype.activeOrderById = function(orderClientId, callback) {
    this._get('order/' + orderClientId, {}, 'private', callback);
};

HitBTCClient.prototype.addOrder = function(obj, callback) {
    this._post('order', obj, callback);
};

HitBTCClient.prototype.cancelOrder = function(clientOrderId, callback) {
    this._delete('order/' + clientOrderId, {}, callback);
};

HitBTCClient.prototype.cancelAllOrders = function(callback) {
    this._delete('order', {}, callback);
};

//wallet information
HitBTCClient.prototype.getBalance = function(callback) {
    this._get('acccount/balance', {}, 'private', callback);
};

module.exports = HitBTCClient;
