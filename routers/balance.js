const hitRestService = require('../exchange_api/hitbtc/hitbtcRest');
const binanceRestService = require('../exchange_api/binance/binanceRest');
const router = require('express').Router();
const responseObj = require('../util/response').response;

router.get('/:exchangeName', (req, res, next) => {
    let exchangeName = req.body.exchangeName;
    if (exchangeName) {
        switch (exchangeName) {
            case 'hitbtc':
                hitRestService.getBalance((err, wallet) => {
                    if (err) {
                        responseObj.message = 'Hitbtc wallet could not retrieved.' + err;
                        responseObj.result = -1;
                        responseObj.data = null;
                    } else if (wallet) {
                        responseObj.message = 'Wallet retrieved successfully.';
                        responseObj.result = 1;

                        wallet = wallet.filter(data => {
                            if (data.available != 0 || data.reserved != 0) {
                                return data;
                            }
                        });
                        responseObj.data = wallet;
                    } else {
                        responseObj.message = 'Hitbtc wallet could not retrieved.';
                        responseObj.result = -1;
                        responseObj.data = null;
                    }
                    res.send(responseObj);
                });
                break;
            case 'binance':
                res.send();
                break;
        }
    }
});

module.exports = router;
