const chainDao = require('../factory/dao/chainDao');
const router = require('express').Router();
const responseObject = require('../util/response').response;

//req.query
//req.params
//req.body
//req.session

// '/chains'
router.get('/:status/:exchangeName/:pageId', (req, res, next) => {
    let status = req.params.status;
    if (status.includes(',')) {
        status = status.split(',').map(Number);
    } else {
        status = parseInt(status);
    }
    let pageId = parseInt(req.params.pageId);
    let exchangeName = req.params.exchangeName;

    chainDao.getChains(status, exchangeName, pageId, (err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Error occured while gettin chains.';
        } else {
            responseObject.data = data;
            responseObject.result = 1;
            responseObject.message = 'chains works well.';
        }
        res.send(responseObject);
    });
});

router.put('/chain', (req, res, next) => {
    let chain = req.body;
    chain.status = parseInt(chain.status);
    chainDao.createChain(chain, (err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Error occured while creating chains.';
        } else {
            responseObject.data = data;
            responseObject.result = 1;
            responseObject.message = 'Chain added.';
        }
        res.send(responseObject);
    });
});

router.post('/chain', (req, res, next) => {
    let chain = req.body;
    chainDao.updateChain(chain, (err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Error occured while updating chain.';
        } else {
            responseObject.data = null;
            responseObject.result = 1;
            responseObject.message = 'Chain updated.';
        }
        res.send(responseObject);
    });
});

router.get('/:chainId/orders', (req, res, next) => {
    let chainId = parseInt(req.params.chainId);
    chainDao.getChainOrders(chainId, (err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Error occured while getting chain orders.';
        } else {
            responseObject.data = data;
            responseObject.result = 1;
            responseObject.message = 'Chain orders well.';
        }
        res.send(responseObject);
    });
});

router.put('/order', (req, res, next) => {
    let order = req.body;
    chainDao.putOrder(order, (err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Error occured while placing order';
        } else {
            responseObject.data = data;
            responseObject.result = 1;
            responseObject.message = 'Order created.';
        }
        res.send(responseObject);
    });
});

router.post('/order', (req, res, next) => {
    let order = req.body;
    chainDao.updateOrder(order, (err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Error occured while updating order';
        } else {
            responseObject.data = data;
            responseObject.result = 1;
            responseObject.message = 'Order updated.';
        }
        res.send(responseObject);
    });
});

module.exports = router;
