const router = require('express').Router();
const symbolDao = require('../factory/dao/symbolDao');
const hitbtcRestApi = require('../exchange_api/hitbtc/hitbtcRest');

const responseObject = require('../util/response').response;

router.get('/:exchangeName', (req, res, next) => {
    let exchangeName = req.params.exchangeName;

    symbolDao.getSymbols(exchangeName, (err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Couldnt get exchangeName:' + exchangeName + ' symbols. Error occured.';
        } else {
            responseObject.data = data;
            responseObject.result = 1;
            responseObject.message = 'exchangeName:' + exchangeName + ' symbols';
        }
        res.send(responseObject);
    });
});

router.get('/hitbtc/refresh', (req, res, next) => {
    let symbols = {};
    let hitClient = new hitbtcRestApi('', '');
    hitClient.symbols((err, data) => {
        if (err || !data) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'Error while getting symbols at exchangeName : Hitbtc ';
            data = JSON.parse(data);
        } else {
            symbolDao.deleteSymbols('hitbtc',(err,rows)=>{
                if(err){
                    console.error(err);
                }else{
                    console.log('symbols deleted for hitbtc');
                    for (let i = 0; i < data.length; i++) {
                        let symbol = {};
                        symbol['quantityIncrement'] = data[i].quantityIncrement;
                        symbol['symbol'] = data[i].id;
                        symbol['minimumBTCamount'] = null;
                        symbol['from_'] = data[i].baseCurrency;
                        symbol['to_'] = data[i].quoteCurrency;
                        symbolDao.putSymbol('hitbtc', symbol, err => {
                            if (err) {
                                console.error(err);
                            }
                        });
                    }
                    responseObject.data = null;
            responseObject.result = 1;
            responseObject.message = 'exchangeName : Hitbtc symbols refreshed successfully.';
                }
                res.send(responseObject);
            });            
        }
        
    });
});

router.get('/hitbtc/refresh/price', (req, res, next) => {
    let hitClient = new hitbtcRestApi('', '');
    hitClient.tickerAll((err, data) => {
        if (err) {
            responseObject.data = null;
            responseObject.result = -1;
            responseObject.message = 'hitbtc tickers couldnt get';
            console.error(err);
        } else {
            for (let i = 0; i < data.length; i++) {
                let symbol = {};
                symbol['exchangeName'] = 'hitbtc';
                symbol['symbol'] = data[i].symbol;
                symbol['lastPrice'] = parseFloat(data[i].last).toFixed(8);
                symbol['lastVolume'] = parseFloat(data[i].volumeQuote);
                symbolDao.updateSymbol(symbol, (err, data) => {
                    if (err) {
                        console.error(err);
                    }
                });
            }
            responseObject.data = null;
            responseObject.result = 1;
            responseObject.message = 'hitbtc tickers updated successfully';
        }

        res.send(responseObject);
    });
});

router.get('/binance/refresh', (req, res, next) => {
    let symbols = {};
});

module.exports = router;
