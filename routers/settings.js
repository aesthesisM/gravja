const router = require('express').Router();
const userDao = require('../factory/dao/userDao');
const settingsDao = require('../factory/dao/settingsDao');
const responseObj = require('../util/response').response;

router.post('/password', (req, res, next) => {
    let password = req.body.password;
    let newPassword = req.body.newPassword;

    let userId = req.session.userId;
    let username = req.session.username;

    userDao.getUser(username, password, (err, data) => {
        if (err || !data) {
            responseObj.data = null;
            responseObj.result = -1;
            responseObj.message = 'User doesnt exist | username or password is wrong.';
        } else {
            userDao.changePassword(userId, newPassword, (err, data) => {
                if (err || !data) {
                    responseObj.data = null;
                    responseObj.result = -1;
                    responseObj.message = 'User password couldnt changed. Error happened.';
                } else {
                    responseObj.data = null;
                    responseObj.result = 1;
                    responseObj.message = 'User password changed successfully.';
                }
                res.send(responseObj);
            });
        }
    });
});

router.get('/apiKeySecret', (req, res, next) => {
    settingsDao.getAllExchangeKeySet((err, data) => {
        if (err || !data) {
            responseObj.data = null;
            responseObj.result = -1;
            responseObj.message = 'Exchange api key-secrets couldnt retrieved. error occured.';
        } else {
            responseObj.data = data;
            responseObj.result = 1;
            responseObj.message = 'Exchange api key-secrets retrieved successfully.';
        }
        res.send(responseObj);
    });
});

router.post('/apiKeySecret', (req, res, next) => {
    let exchangeName = req.body.exchangeName;
    let apiKey = req.body.apiKey;
    let apiSecret = req.body.apiSecret;

    settingsDao.updateExchangeKeySet(exchangeName, apiKey, apiSecret, (err, data) => {
        if (err || !data) {
            responseObj.data = null;
            responseObj.result = -1;
            responseObj.message = 'exchange api key-secret couldnt updated. error occured';
        } else {
            responseObj.data = null;
            responseObj.result = 1;
            responseObj.message = 'exchange api key-secret successfully updated.';
        }
        res.send(responseObj);
    })
});

module.exports = router;