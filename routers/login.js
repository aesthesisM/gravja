const router = require('express').Router();
const userDao = require('../factory/dao/userDao');
const jwt = require('jsonwebtoken');
const jwtSecret = 'Gravja.Token';
const responseObject = require('../util/response').response;
router.get('/', (req, res, next) => {
    console.log('welcome route');
    res.render('login');
});

router.post('/login', (req, res, next) => {
    let username = req.body.username;
    let password = req.body.password;
    userDao.getUser(username, password, (err, user) => {
        if (err || !user) {
            console.log('user not found');
            responseObject.data = null;
            responseObject.message = 'User may not exist | Username | password is wrong.';
            responseObject.result = -1;
        } else {
            console.log('user logged.' + JSON.stringify(user));
            req.session.authenticated = true;
            req.session.userId = user.id;
            req.session.username = user.username;
            req.session.token = jwt.sign(req.session.username, jwtSecret);

            responseObject.data = null;
            responseObject.message = 'Login ok';
            responseObject.result = 1;
        }
        res.send(responseObject);
    });
});

router.get('/logout', (req, res, next) => {
    if (req.session) {
        req.session.destroy(err => {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        });
    }
});

router.get('/token', (req, res, next) => {
    if (req.session) {
        res.send(req.session.token);
    } else {
        res.send(null);
    }
});

module.exports = router;
