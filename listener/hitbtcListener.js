const crypto = require('crypto');
const WebSocket = require('ws');
const async = require('async');
const chainDao = require('../factory/dao/chainDao');
const settingsDao = require('../factory/dao/settingsDao');
const symbolDao = require('../factory/dao/symbolDao');

const hitBTCSocketUrl = 'wss://api.hitbtc.com/api/2/ws';
const orderSignature = 'Gravja_';

const orderListener = new wsOrderListener(orderManager);

function wsOrderListener(callback) {
    var ws = null;
    ws = new WebSocket(hitBTCSocketUrl, null, {
        handshakeTimeout: 3000
    });

    ws.on('open', () => {
        console.log('wsOrderListener opened.');
    });

    ws.on('close', () => {
        console.log('wsOrderListener closed.');
        setTimeout(function () {
            ws = new WebSocket(hitBTCSocketUrl, null, {
                handshakeTimeout: 3000
            });
        }, 500);
    });

    ws.on('error', err => {
        callback(err);
        console.error('wsOrderListener error :' + err);
    });

    ws.on('message', data => {
        callback(null, data);
    });
    this.ws = ws;
}

wsOrderListener.prototype._authorize = function (data) {
    if (this.ws.readyState == 1) {
        this.ws.send(JSON.stringify(data));
    } else {
        setTimeout(this._authorize(data), 1000);
    }
};

wsOrderListener.prototype._listenOrders = function () {
    //order status = new, suspended, partiallyFilled, filled, canceled, expired
    var obj = {
        method: 'subscribeReports',
        params: {},
        id: 'Gravja order listener'
    };
    if (this.ws.readyState == 1) {
        this.ws.send(JSON.stringify(obj));
    } else {
        setTimeout(this._listenOrders, 1000);
    }
};

wsOrderListener.prototype._placeOrder = function (order) {
    order.clientOrderId = new Date().getTime() + orderSignature + order.id + '-' + order.chainId;
    let obj = {
        method: 'newOrder',
        params: {
            clientOrderId: order.clientOrderId,
            symbol: order.market,
            side: order.buysell,
            price: order.price,
            quantity: order.amount
        },
        id: 'Gravja place order'
    };
    if (this.ws.readyState == 1) {
        this.ws.send(JSON.stringify(obj));
    } else {
        setTimeout(this._placeOrder(order), 1000);
    }

};

wsOrderListener.prototype._cancelOrder = function (id) {
    let obj = {
        method: 'cancelOrder',
        params: {
            clientOrderId: id
        }
    };

    if (this.ws.readyState == 1) {
        this.ws.send(JSON.stringify(obj));
    } else {
        setTimeout(this._cancelOrder(id), 1000);
    }
};

wsOrderListener.prototype._balance = function () {
    let obj = {
        method: 'getTradingBalance',
        params: {},
        id: 'Gravja balance.'
    };
    if (this.ws.readyState == 1) {
        this.ws.send(JSON.stringify(obj));
    } else {
        setTimeout(this._balance, 1000);
    }
};

wsOrderListener.prototype._close = function () {
    this.ws.close();
};

function HitBTCAuth(APIKey, APISecret) {
    var authObj = {};
    authObj.method = 'login';
    authObj.params = {};
    authObj.params.algo = 'HS256';
    authObj.params.pKey = APIKey;
    authObj.params.nonce = Date.now().toString();
    authObj.params.signature = crypto
        .createHmac('sha256', APISecret)
        .update(authObj.params.nonce, 'utf8')
        .digest('hex');

    this.auth = authObj;
}

const _has = (object, key) => {
    return object ? hasOwnProperty.call(object, key) : false;
};
const tryParseInt = str => {
    var result = -1;
    if (str !== null) {
        if (str.length > 0) {
            if (!isNaN(str)) {
                result = parseInt(str);
            }
        }
    }
    return result;
};

function orderManager(err, socketData) {
    if (socketData != null) {
        socketData = JSON.parse(socketData);
        if (err) {
            //socket error
            console.error(err);
        } else if (_has(socketData, 'error')) {
            //socket api order error
            if (socketData.error.code == 20001) {
                errSocketCallback({ exchangeName: 'hitbtc', code: socketData.error.code, message: socketData.error.message, description: socketData.error.description + ' delete that chain!!!!' });
                console.error(socketData.error.message + ', ' + socketData.error.description);
            } else {
                console.error(socketData.error.message + ',' + socketData.error.description);
            }
        } else if (_has(socketData, 'method') && socketData.method == 'activeOrders') {
            console.log('site awakening orders : ' + JSON.stringify(socketData));
            //cancel all orders and chained running before.
            chainDao.getAllOrderedChainOrders((err, orders) => {
                if (err) {
                    console.error(err);
                } else if (orders) {
                    async.each(
                        orders,
                        function (order) {
                            chainDao.orderStatusUpdate(order.id, 2, err => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    orderListener._cancelOrder(order.clientOrderId);
                                    chainDao.chainStatusUpdate(order.chainId, 3, err => {
                                        if (err) {
                                            console.error(err);
                                        }
                                    });
                                }
                            });
                        },
                        function (err) {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log('all orders cancelled.');
                            }
                        }
                    );
                }
            });

            chainDao.getRunningChains((err, chains) => {
                if (err) {
                    console.error(err);
                } else if (chains) {
                    async.each(
                        chains,
                        function (chain) {
                            chainDao.chainStatusUpdate(chain.id, 3, err => {
                                if (err) {
                                    console.error(err);
                                } else {
                                }
                            });
                        },
                        function (err) {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log('all chains stopped.');
                            }
                        }
                    );
                }
            });
        } else if (_has(socketData, 'method') && socketData.method === 'report') {
            //check order status if the socketData is about orders
            if (socketData.params.clientOrderId.includes(orderSignature)) {
                //chain order

                let chainOrderId = tryParseInt(
                    socketData.params.clientOrderId.substring(socketData.params.clientOrderId.indexOf('_') + 1, socketData.params.clientOrderId.indexOf('-'))
                );
                let chainId = tryParseInt(socketData.params.clientOrderId.substring(socketData.params.clientOrderId.indexOf('-') + 1));
                let nextOrder = null;
                let currentOrder = null;
                if (socketData.params.status === 'filled') {
                    //if chain order completed
                    console.log('order filled :' + JSON.stringify(socketData.params));
                    chainDao.getOrder(chainOrderId, (err, order) => {
                        if (err) {
                            console.error(err);
                        } else {
                            currentOrder = order;
                            //set order completed
                            currentOrder.status = 1;
                            currentOrder.completed_time = new Date();
                            chainDao.updateOrder(currentOrder, (err, data) => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    chainDao.putHappenedOrder(currentOrder.id, (err, data) => {
                                        if (err) {
                                            console.error(err);
                                        }
                                    });
                                    chainDao.getChain(currentOrder.chainId, (err, chain) => {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            if (chain.end_time != null && chain.end_time > new Date()) {
                                                //if chain has end time and if its passed then close the chain.
                                                chain.status = 3;
                                                chainDao.updateChain(chain, (err, data) => {
                                                    if (err) {
                                                        console.error(err);
                                                    }
                                                });
                                            } else if (chain.repeatable) {
                                                //if chain is repeatable then place other order
                                                chainDao.getWaitingRecursiveOrder(currentOrder.chainId, currentOrder.id, (err, order) => {
                                                    if (err) {
                                                        console.error(err);
                                                    } else {
                                                        orderListener._placeOrder(order);
                                                        chainDao.updateOrder(order, (err, data) => {
                                                            if (err) {
                                                                console.error(err);
                                                            }
                                                        });
                                                    }
                                                });
                                            } else {
                                                //call the next order :=)
                                                chainDao.getChainNextOrder(currentOrder.chainId, currentOrder.order_, (err, next) => {
                                                    if (err) {
                                                        console.error(err);
                                                    } else if (next) {
                                                        nextOrder = next;
                                                        orderListener._placeOrder(nextOrder);
                                                        chainDao.updateOrder(nextOrder, (err, data) => {
                                                            if (err) {
                                                                console.error(err);
                                                            }
                                                        });
                                                    } else {
                                                        //if there is not any next order then complete chain
                                                        chainDao.chainStatusUpdate(chainId, 2, (err, data) => {
                                                            if (err) {
                                                                console.error(err);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                        getBalance();
                                    });
                                }
                            });
                        }
                    });
                } else if (socketData.params.status === 'partiallyFilled') {
                    console.log('partiallyFilled order :' + JSON.stringify(socketData));
                    getBalance();
                } else if (socketData.params.status === 'canceled') {
                    //2 = stopped
                    chainDao.orderStatusUpdate(chainOrderId, 2, (err, data) => {
                        if (err) {
                            console.error(err);
                        } else {
                            chainDao.chainStatusUpdate(chainId, 3, (err, data) => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    console.log('order cancelled' + JSON.stringify(socketData));
                                }
                            });
                        }
                        getBalance();
                    });
                    //if chain order cancelled
                }
            }
        } else if (_has(socketData, 'result') && socketData.result != null && socketData.result instanceof Array) {
            //balance information
            console.log('balance : ');

            let currentBalance = socketData.result.filter(data => {
                if (data.available != 0 || data.reserved != 0) {
                    return data;
                }
            });
            if (balanceSocketCallback != null) {
                balanceSocketCallback({ exchangeName: 'hitbtc', balance: currentBalance });
            }
            console.log(JSON.stringify(currentBalance));
        } else if (_has(socketData, 'method') && socketData.method == 'snapshotCandles') {
            // subscribeCandles methods data
            //socketData.params.data to bulk insert pattern.
            //every data for bulk insert must be in array format
            for (let i = 0; socketData.params.data.length; i++) {
                if (socketData.params.data[i] != null && socketData.params.data[i] != undefined) {
                    //let result = [socketData.params.data[i].timestamp,socketData.params.data[i].max,socketData.params.data[i].min,socketData.params.data[i].open,socketData.params.data[i].close,socketData.params.data[i].volume];
                    //console.log(result);
                }
            }
            console.log(socketData.params.data.length + '-' + socketData.params.symbol + '-' + socketData.params.period);

            // transfer this socketData.params.data to array of arrays
            //change historicalDataDao's insert method to bulk insert
        } else if (_has(socketData, 'method') && socketData.method == 'updateCandles') {
            //console.log(JSON.stringify(socketData));
            //after getting whole history, no need to listen updates
            orderListener.unsubscribeSymbol(socketData.data.symbol, socketData.data.period);
        }
    }
}

//socket chainId
const startChain = chainId => {
    chainDao.getChainFirstOrder(chainId, (err, row) => {
        if (err) {
            console.error(err);
        } else {
            orderListener._placeOrder(row);
            chainDao.updateOrder(row, (err, data) => {
                if (err) {
                    console.error(err);
                }
            });
        }
    });
};

//socket chainId,status

const stopDeleteChain = (chainId, status) => {
    chainDao.chainStatusUpdate(chainId, status, (err, data) => {
        if (err) {
            console.error(err);
        } else {
            chainDao.getOrderedFirstChainOrder(chainId, (err, row) => {
                if (err) {
                    console.error(err);
                } else if (row != undefined && row != null) {
                    orderListener._cancelOrder(row.clientOrderId);
                    chainDao.orderStatusUpdate(row.id, 3, (err, data) => {
                        if (err) {
                            console.error(err);
                        }
                    });
                } else {
                }
            });
        }
    });
};

//server.js method

let balanceSocketCallback = null;
let errSocketCallback = null;

const setErrorSocketCallback = callback => {
    errSocketCallback = callback;
};
const setBalanceSocketCallback = callback => {
    balanceSocketCallback = callback;
};
const getBalance = () => {
    orderListener._balance();
};
wsOrderListener.prototype.subscribeSymbol = function (symbol, period) {
    var obj = {
        method: 'subscribeCandles',
        params: {
            symbol: symbol,
            period: period
        },
        id: 'Gravja subscribeCandles'
    };
    if (this.ws.readyState == 1) {
        this.ws.send(JSON.stringify(obj));
    } else {
        setTimeout(this.subscribeSymbol(symbol, period), 1000);
    }
};

wsOrderListener.prototype.unsubscribeSymbol = function (symbol, period) {
    var obj = {
        method: 'unsubscribeCandles',
        params: {
            symbol: symbol,
            period: period
        },
        id: ''
    };
    if (this.ws.readyState == 1) {
        this.ws.send(JSON.stringify(obj));
    } else {
        setTimeout(this.unsubscribeSymbol(symbol, period), 1000);
    }
};

module.exports = {
    startChain,
    stopDeleteChain,
    getBalance,
    setBalanceSocketCallback,
    setErrorSocketCallback
};

setTimeout(function () {
    settingsDao.getExchangeKeySet('hitbtc', (err, keySet) => {
        if (err) {
            console.error(err);
        } else {
            if (keySet.apiKey != null && keySet.apiKey != undefined) {
                let hitAut = new HitBTCAuth(keySet.apiKey, keySet.apiSecret);
                orderListener._authorize(hitAut.auth);
                orderListener._listenOrders();
            } else {
                console.error('hitbtc apikey-secret pair must be given...');
            }
        }
    });
}, 1000);