//main frameworks
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const morgan = require('morgan');
const handleBars = require('express-handlebars');
const path = require('path');
//conf file .env
require('dotenv').config();
process.env.TZ = 'UTC';
var conf = process.env;
//routers
//login
const router_login = require('./routers/login');
//settings
const router_settings = require('./routers/settings');
//chain
const router_chain = require('./routers/chain');
//symbols
const router_symbols = require('./routers/symbols');
//balance
const router_balance = require('./routers/balance');

var app = express();

//for now
app.set('env', conf.ENVIRONMENT);

//express-session config
const sessionProperties = {
    secret: 'Gravja For AesthesisM',
    resave: true,
    saveUninitialized: false,
    cookie: { secure: false, maxAge: null },
    name: 'Gravja.sid'
};

if (app.get('env') === 'production') {
    app.set('trust proxy', 1); // trust first proxy
    //sessionProperties.cookie.secure = true // serve secure cookies https
}
app.use(session(sessionProperties));
//authentication check
const checkAuth = (req, res, next) => {
    if ((req.url.includes('/pages/') || req.url.includes('/api/')) && (!req.session || !req.session.authenticated)) {
        res.redirect('/');
    } else {
        next();
    }
};
app.use(checkAuth);

app.set('trust proxy', 1); //trust first proxy

//logging the requests
app.use(morgan('dev'));
//json parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//view engine
//// view engine setup
app.engine(
    'html',
    handleBars({
        extname: 'html',
        defaultLayout: 'login',
        layoutsDir: path.join(__dirname, '/public/pages/')
    })
);

app.set('views', path.join(__dirname, 'public/pages'));
app.set('view engine', 'html');
//statics
app.use(express.static(__dirname + '/public/'));
//routers
app.use('/', router_login);
app.use('/api/chains', router_chain);
app.use('/api/symbols', router_symbols);
app.use('/api/settings', router_settings);
app.use('/api/balance', router_balance);

module.exports = app;
