const symbolDao = require('../factory/dao/symbolDao');
const hitbtcRestApi = require('../exchange_api/hitbtc/hitbtcRest');
const hitPeriod = ['H1', 'D1'];

let hitClient = new hitbtcRestApi('', '');
const getHistoricalData = () => {
    symbolDao.getHistoricalSymbols('hitbtc', (err, symbols) => {
        if (err) {
            console.error(err);
        } else if (symbols != null && symbols.length > 0) {
            for (let i = 0; i < symbols.length; i++) {
                setTimeout(function() {
                    
                    hitClient.candlesWithParameters(symbols[i].symbol, hitPeriod[0], 1000, (err, data) => {
                        if (err) {
                            return console.error(err);
                        } else {
                            console.log(data.length + '|' + data.symbol + '|' + data.period);
                        }
                    });

                    hitClient.candlesWithParameters(symbols[i].symbol, hitPeriod[1], 1000, (err, data) => {
                        if (err) {
                            return console.error(err);
                        } else {
                            console.log(data.length + '|' + data.symbol + '|' + data.period);
                        }
                    });
                }, i * 4000);
            }
        }
    });
};

const runPeriodicalChunk = () => {
    //get all symbols
    //loop over symbols for historical data
    //chunk historical data in 5,10,20,40
    //find min,max for every chunk
    //put them db
};

getHistoricalData();
setTimeout(runPeriodicalChunk, 15 * 60 * 100);

setInterval(function() {
    getHistoricalData(), setTimeout(runPeriodicalChunk, 30 * 60 * 1000);
}, 1000 * 60 * 60 * 24);
