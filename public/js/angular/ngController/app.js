var gravjaApp = angular.module('gravjaApp', ['ngSanitize', 'ngRoute', 'ui.select']);
//#routes
gravjaApp.config($routeProvider => {
    $routeProvider
        .when('/ui/hitbtc/chain', {
            controller: 'HitbtcController as hitCtrl',
            templateUrl: 'hitbtc/chain.html'
        })
        .when('/ui/hitbtc/order', {
            controller: 'HitbtcController as hitCtrl',
            templateUrl: 'hitbtc/order.html'
        })
        .when('/ui/hitbtc/balance', {
            controller: 'HitbtcController as hitCtrl',
            templateUrl: 'hitbtc/balance.html'
        })
        .when('/ui/binance/chain', {
            controller: 'BinanceController as binCtrl',
            templateUrl: 'binance/chain.html'
        })
        .when('/ui/binance/order', {
            controller: 'BinanceController as binCtrl',
            templateUrl: 'binance/order.html'
        })
        .when('/ui/binance/balance', {
            controller: 'BinanceController as binCtrl',
            templateUrl: 'binance/balance.html'
        })
        .when('/ui/settings', {
            controller: 'SettingsController as setCtrl',
            templateUrl: 'settings.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});
//#services

gravjaApp.service('LoginService', function($http) {
    this.getToken = () => {
        return $http({
            method: 'GET',
            url: '/token'
        });
    };
});

gravjaApp.service('SettingsService', function($http) {
    this.getExchangeApiKeySecret = () => {
        return $http({
            method: 'GET',
            url: '/api/settings/apiKeySecret'
        });
    };

    this.updateExchangeApiKeySecret = (exchangeName, apiKey, apiSecret) => {
        return $http({
            method: 'POST',
            url: '/api/settings/apiKeySecret',
            data: { exchangeName, apiKey, apiSecret }
        });
    };

    this.updateUserPassword = (password, newPassword) => {
        return $http({
            method: 'POST',
            url: '/api/settings/password',
            data: { password, newPassword }
        });
    };

    this.updateSymbols = exchangeName => {
        return $http({
            method: 'GET',
            url: '/api/symbols/' + exchangeName + '/refresh'
        });
    };

    this.updateSymbolsPrice = exchangeName => {
        return $http({
            method: 'GET',
            url: '/api/symbols/' + exchangeName + '/refresh/price'
        });
    };
});
gravjaApp.service('ChainOrderService', function($http) {
    this.getSymbols = exchangeName => {
        return $http({
            method: 'GET',
            url: '/api/symbols/' + exchangeName
        });
    };

    this.getActiveChains = (exchangeName, pageId) => {
        let status = '0,1';
        return $http({
            method: 'GET',
            url: '/api/chains/' + status + '/' + exchangeName + '/' + pageId
        });
    };

    this.createChain = chain => {
        return $http({
            method: 'PUT',
            url: '/api/chains/chain',
            data: chain
        });
    };

    this.updateChain = chain => {
        return $http({
            method: 'POST',
            url: '/api/chains/chain',
            data: chain
        });
    };

    this.getChainOrders = chainId => {
        return $http({
            method: 'GET',
            url: '/api/chains/' + chainId + '/orders'
        });
    };

    this.addOrder = order => {
        return $http({
            method: 'PUT',
            url: '/api/chains/order',
            data: order
        });
    };

    this.updateOrder = order => {
        return $http({
            method: 'POST',
            url: '/api/chains/order',
            data: order
        });
    };
});

gravjaApp.service('BalanceService', function($http) {
    this.getBalance = exchangeName => {
        return $http({
            method: 'GET',
            url: '/api/balance/' + exchangeName
        });
    };
});
var binanceBalance = null;
var hitbtcBalance = null;
var socket = null;

const connectToSocket = token => {
    socket = io.connect(
        'http://localhost:8080',
        { query: 'token=' + token }
    );

    socket.on('connection', data => {
        console.log('connected to socket');
    });

    socket.on('balance', data => {
        //console.log(data);
        switch (data.exchangeName) {
            case 'hitbtc':
                hitbtcBalance = data.balance;
                break;
            case 'binance':
                break;
        }
    });

    socket.on('err', err => {
        alert(JSON.stringify(err));
    });
};
