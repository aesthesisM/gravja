gravjaApp.controller("SettingsController", function(
	$scope,
	$http,
	$sce,
	$q,
	SettingsService
) {
	var setCtrl = this;

	setCtrl.password = null;
	setCtrl.newPassword = null;
	setCtrl.exchanges = [];

	SettingsService.getExchangeApiKeySecret().then(response => {
		response = response.data;
		if (response.result == 1) {
			setCtrl.exchanges = response.data;
			console.log(response.message);
		} else {
			console.error(response.message);
		}
		console.log(setCtrl.exchanges);
	});

	setCtrl.updateUserPassword = () => {
		SettingsService.updateUserPassword(
			setCtrl.password,
			setCtrl.newPassword
		).then(response => {
			response = response.data;
			if (response.result == 1) {
				console.log(response.message);
			} else {
				console.error(response.message);
			}
		});
	};

	setCtrl.updateExchangeApiKeySecret = index => {
		let ex = setCtrl.exchanges[index];
		SettingsService.updateExchangeApiKeySecret(
			ex.exchangeName,
			ex.apiKey,
			ex.apiSecret
		).then(response => {
			response = response.data;
			if (response.result == 1) {
				console.log(response.message);
			} else {
				console.error(response.message);
			}
		});
	};

	setCtrl.updateSymbols = index => {
		let ex = setCtrl.exchanges[index];
		SettingsService.updateSymbols(ex.exchangeName).then(response => {
			SettingsService.updateSymbolsPrice(ex.exchangeName).then(resp => {
				resp = resp.data;
				if (resp.result == 1) {
					console.log(resp.message);
				} else {
					console.error(resp.message);
				}
			});
		});
	};
});
