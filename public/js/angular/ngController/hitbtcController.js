gravjaApp.controller('HitbtcController', function($scope, $http, $sce, $q, ChainOrderService, LoginService) {
    let exchangeName = 'hitbtc';
    let hitCtrl = this;
    LoginService.getToken().then(response => {
        if (response.data != null) {
            connectToSocket(response.data);
            socket.emit('balance', 'hitbtc');
            setTimeout(function() {
                hitCtrl.balance = hitbtcBalance;
            }, 500);
        }
    });

    hitCtrl.tradingViewURL = null;
    hitCtrl.currentPage = 0;
    hitCtrl.showLimit = 20;
    hitCtrl.symbols = [];
    hitCtrl.activeChains = [];

    hitCtrl.addChainModel = {};
    hitCtrl.addChainModel.status = 0;
    hitCtrl.addChainModel.repeatable = 0;
    hitCtrl.addChainModel.exchangeName = exchangeName;

    hitCtrl.currentChain = {};
    hitCtrl.currentChain.status = 0;
    hitCtrl.currentChain.exchangeName = exchangeName;

    hitCtrl.addOrderModel = {};
    hitCtrl.currentOrder = {};

    ChainOrderService.getSymbols(exchangeName).then(response => {
        response = response.data;
        if (response.result == 1) {
            hitCtrl.symbols = response.data;
        } else {
            console.error(response.message);
        }
        console.log(hitCtrl.symbols);
    });

    ChainOrderService.getActiveChains(exchangeName, hitCtrl.currentPage).then(response => {
        response = response.data;
        if (response.result == 1) {
            hitCtrl.activeChains = response.data;
        } else {
            console.error(response.message);
        }
        console.log(hitCtrl.activeChains);
    });

    hitCtrl.showAddChainModal = () => {
        $('#modal-addChain').modal('toggle');
    };

    hitCtrl.updateChainModal = index => {
        hitCtrl.currentChain = hitCtrl.activeChains[index];
        hitCtrl.currentChain.repeatable = hitCtrl.currentChain.repeatable != 1 ? false : true;
        hitCtrl.currentChain.endTime = new Date(hitCtrl.currentChain.end_time);
        $('#modal-updateChain').modal('toggle');
    };

    hitCtrl.showAddOrderModal = () => {
        hitCtrl.currentOrder = {};
        $('#modal-addOrder').modal('toggle');
    };

    hitCtrl.showUpdateOrderModal = index => {
        hitCtrl.addOrderModel = {};
        hitCtrl.currentOrder = hitCtrl.currentChain.orders[index];
        $('#modal-updateOrder').modal('toggle');
    };

    hitCtrl.getChainOrders = chain => {
        hitCtrl.currentChain = chain;
        ChainOrderService.getChainOrders(hitCtrl.currentChain.id).then(response => {
            response = response.data;
            if (response.result == 1) {
                hitCtrl.currentChain.orders = response.data;
                console.log(hitCtrl.currentChain);
            } else {
                console.error(response.message);
            }
        });
    };

    hitCtrl.addChain = () => {
        if (hitCtrl.addChainModel.endTime != null) {
            let userTimezoneOffset = hitCtrl.addChainModel.endTime.getTimezoneOffset() * 60000;
            hitCtrl.addChainModel.end_time = new Date(hitCtrl.addChainModel.endTime.getTime() + userTimezoneOffset * -1);
            hitCtrl.addChainModel.end_time = moment(hitCtrl.addChainModel.end_time).format('YYYY-MM-DD');
        }
        ChainOrderService.createChain(hitCtrl.addChainModel).then(response => {
            response = response.data;
            if (response.result == 1) {
                hitCtrl.addChainModel.id = response.data;
                hitCtrl.activeChains.push(hitCtrl.addChainModel);
                hitCtrl.addChainModel = {};
                hitCtrl.addChainModel.status = 0;
                hitCtrl.addChainModel.exchangeName = exchangeName;
                hitCtrl.addChainModel.repeatable = 0;
            } else {
                console.error(response.message);
            }
            console.log(hitCtrl.currentChain);
        });
    };

    hitCtrl.updateChain = (job, index) => {
        //update currentChain
        if (job == undefined) {
            let userTimezoneOffset = hitCtrl.currentChain.endTime.getTimezoneOffset() * 60000;
            hitCtrl.currentChain.end_time = new Date(hitCtrl.currentChain.endTime.getTime() + userTimezoneOffset * -1);
            hitCtrl.currentChain.end_time = moment(hitCtrl.currentChain.end_time).format('YYYY-MM-DD');
            ChainOrderService.updateChain(hitCtrl.currentChain).then(response => {
                response = response.data;
                if (response.result == 1) {
                } else {
                    console.error(response.message);
                }
                console.log(hitCtrl.currentChain);
            });
        } else {
            //start stop delete
            hitCtrl.currentChain = hitCtrl.activeChains[index];
            switch (job) {
                case 'start':
                    hitCtrl.currentChain.status = 1;
                    ChainOrderService.updateChain(hitCtrl.currentChain).then(response => {
                        response = response.data;
                        if (response.result == 1) {
                            socket.emit('startChain', exchangeName, hitCtrl.currentChain.id);
                        } else {
                            console.error(response.message);
                        }
                        console.log(hitCtrl.currentChain);
                    });
                    break;
                case 'stop':
                    hitCtrl.currentChain.status = 3;
                    ChainOrderService.updateChain(hitCtrl.currentChain).then(response => {
                        response = response.data;
                        if (response.result == 1) {
                            socket.emit('stopDeleteChain', exchangeName, hitCtrl.currentChain.id, 3);
                        } else {
                            console.error(response.message);
                        }
                        console.log(hitCtrl.currentChain);
                    });
                    break;
                case 'delete':
                    hitCtrl.currentChain.status = 4;
                    ChainOrderService.updateChain(hitCtrl.currentChain).then(response => {
                        response = response.data;
                        if (response.result == 1) {
                            socket.emit('stopDeleteChain', exchangeName, hitCtrl.currentChain.id, 4);
                        } else {
                            console.error(response.message);
                        }
                        console.log(hitCtrl.currentChain);
                    });
                    break;
            }
        }
    };

    hitCtrl.addOrder = () => {
        hitCtrl.addOrderModel.err = '';
        /*
		validation for amount and quantityIncrement
		*/
        let amount = parseFloat(hitCtrl.addOrderModel.amount).toFixed(8) * 10000;
        let quantityIncrement = parseFloat(hitCtrl.addOrderModel.symbol.quantityIncrement).toFixed(8) * 10000;
        if ((amount % quantityIncrement) / 10000 == 0) {
            hitCtrl.addOrderModel.chainId = hitCtrl.currentChain.id;
            hitCtrl.addOrderModel.order_ = hitCtrl.currentChain.orders != null ? hitCtrl.currentChain.orders.length + 1 : 1;
            hitCtrl.addOrderModel.exchangeName = exchangeName;
            hitCtrl.addOrderModel.from_ = hitCtrl.addOrderModel.symbol.from_;
            hitCtrl.addOrderModel.to_ = hitCtrl.addOrderModel.symbol.to_;
            hitCtrl.addOrderModel.market = hitCtrl.addOrderModel.symbol.symbol;
            ChainOrderService.addOrder(hitCtrl.addOrderModel).then(response => {
                response = response.data;
                if (response.result == 1) {
                    hitCtrl.addOrderModel = {};
                    hitCtrl.getChainOrders(hitCtrl.currentChain);
                } else {
                    hitCtrl.addOrderModel.err = response.message;
                }
            });
        } else {
            hitCtrl.addOrderModel.err = 'amount should be dividible to quantity Increment of coin';
        }
    };

    hitCtrl.updateOrder = (order, to, index, deleted) => {
        //check for deleted first
        if (deleted != undefined && order != undefined) {
            //set deleted
            order.status = 3;
            ChainOrderService.updateOrder(order).then(response => {
                response = response.data;
                if (response.result == -1) {
                    return console.error(response.message);
                } else {
                    console.log('order updated successfully.');
                }
            });
        } else if (to != undefined && index != undefined) {
            let toUpped = null;
            let toDowned = null;
            switch (to) {
                case 'up':
                    toUpped = hitCtrl.currentChain.orders[index];
                    toDowned = hitCtrl.currentChain.orders[index - 1];

                    toUpped.order_ = toUpped.order_ - 1;
                    toDowned.order_ = toDowned.order_ + 1;

                    ChainOrderService.updateOrder(toUpped).then(response => {
                        response = response.data;
                        if (response.result == -1) {
                            return console.error(response.message);
                        } else {
                            ChainOrderService.updateOrder(toDowned).then(resp => {
                                resp = resp.data;
                                if (resp.result == -1) {
                                    return console.error(resp.message);
                                } else {
                                    hitCtrl.getChainOrders(hitCtrl.currentChain);
                                }
                            });
                        }
                    });
                    break;
                case 'down':
                    toUpped = hitCtrl.currentChain.orders[index];
                    toDowned = hitCtrl.currentChain.orders[index + 1];

                    toUpped.order_ = toUpped.order_ + 1;
                    toDowned.order_ = toDowned.order_ - 1;

                    ChainOrderService.updateOrder(toUpped).then(response => {
                        response = response.data;
                        if (response.result == -1) {
                            return console.error(response.message);
                        } else {
                            ChainOrderService.updateOrder(toDowned).then(resp => {
                                resp = resp.data;
                                if (resp.result == -1) {
                                    return console.error(resp.message);
                                } else {
                                    hitCtrl.getChainOrders(hitCtrl.currentChain);
                                }
                            });
                        }
                    });
                    break;
            }
        } else if (order != undefined) {
            order.from_ = order.symbol.from_;
            order.to_ = order.symbol.to_;
            order.market = order.symbol.symbol;
            ChainOrderService.updateOrder(order).then(response => {
                response = response.data;
                if (response.result == -1) {
                    return console.error(response.message);
                } else {
                    console.log('order updated successfully');
                }
            });
        }
    };

    //watchers
    $scope.$watchGroup(['hitCtrl.addOrderModel.symbol', 'hitCtrl.currentOrder.symbol', 'hitCtrl.balance'], function(newValues, oldValues, scope) {
        if ((hitCtrl.addOrderModel != null && hitCtrl.addOrderModel.symbol != undefined) || (hitCtrl.currentOrder != null && hitCtrl.currentOrder.symbol != null)) {
            //set price the symbols last price
            //edit tradingView url by symbol and exchange
            if (hitCtrl.addOrderModel != null && hitCtrl.addOrderModel.symbol != undefined) {
                hitCtrl.addOrderModel.price = hitCtrl.addOrderModel.symbol.lastPrice;

                hitCtrl.tradingViewURL = $sce.trustAsResourceUrl('/pages/tradingView.html?site=HITBTC&value=' + hitCtrl.addOrderModel.symbol.symbol);
            } else if (hitCtrl.currentOrder != null && hitCtrl.currentOrder.symbol != null) {
                hitCtrl.currentOrder.price = hitCtrl.currentOrder.symbol.lastPrice;
                hitCtrl.tradingViewURL = $sce.trustAsResourceUrl('/pages/tradingView.html?site=HITBTC&value=' + hitCtrl.currentOrder.symbol.symbol);
            }
        }
    });
});
