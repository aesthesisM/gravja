jQuery(document).ready(function () {


    $('.login-form').on('submit', function (e) {

        $(this).find('input[type="text"], input[type="password"], textarea').each(function () {
            if ($(this).val() == "") {
                e.preventDefault();
                $(this).addClass('input-error');
            }
            else {
                $(this).removeClass('input-error');
            }
        });
        $.post("/login", $(this).serialize())
            .done(function (data) {
                console.log(data);
                if (data.result === 1) {
                    location.href = "pages/index.html";
                } else {
                    alert("nop");
                }
            });
        e.preventDefault();
    });
});