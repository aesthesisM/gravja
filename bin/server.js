const app = require('../app');
const server = require('http').Server(app);
const port = 8080;
const jwtSecret = 'Gravja.Token';
app.set('port', port);
const hitbtcListener = require('../listener/hitbtcListener');
const socketIO = require('socket.io')(server);
const socketIOJWT = require('socketio-jwt');

socketIO.set(
    'authorization',
    socketIOJWT.authorize({
        secret: jwtSecret,
        handshake: true
    })
);

socketIO.sockets.on('connection', socket => {
    console.log('user connected to socket');
    socket.emit('connection', 'connected');

    socket.on('startChain', (exchangeName, chainId) => {
        switch (exchangeName) {
            case 'hitbtc':
                hitbtcListener.startChain(chainId);
                break;
            case 'binance':
                break;
        }
        console.log('chainId:' + chainId);
    });

    socket.on('stopDeleteChain', (exchangeName, chainId, status) => {
        switch (exchangeName) {
            case 'hitbtc':
                hitbtcListener.stopDeleteChain(chainId, status);
                break;
            case 'binance':
                break;
        }
        console.log('startStopChain chainid : ' + chainId + ', status:' + status);
    });

    socket.on('balance', exchangeName => {
        switch (exchangeName) {
            case 'hitbtc':
                hitbtcListener.getBalance();
                break;
            case 'binance':
                break;
        }
    });
});

const errorCallback = err => {
    socketIO.emit('err', err);
};

const balanceCallback = data => {
    socketIO.emit('balance', data);
};

hitbtcListener.setBalanceSocketCallback(balanceCallback);
hitbtcListener.setErrorSocketCallback(errorCallback);
server.listen(port, 'localhost', function() {
    var host = server.address().address;
    console.log('server listening at port:' + port + ' and  host is :' + host);
});
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    //debug('Listening on ' + bind);
}
