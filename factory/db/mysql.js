const mysql = require("mysql");
require("dotenv").config();
var conf = process.env;

const dbConf = {
  host: conf.DB_HOST,
  user: conf.DB_USER,
  password: conf.DB_PASSWORD,
  port: conf.DB_PORT,
  database: conf.DB_NAME,
  multipleStatements: true,
  dateStrings: true,
  acquireTimeout: 900000 //15 min
};

const getConnection = () => {
  return mysql.createConnection(dbConf);
};

const closeConnection = conn => {
  conn.end();
};

const executeSQL = (sql, params, callback) => {
  let conn = getConnection();
  try {
    conn.query(sql, params, (err, rows) => {
      if (err) {
        callback(err);
      } else {
        callback(null, rows);
      }
      conn.end();
    });
  } catch (e) {
    console.log(sql);
    console.log(params);
    console.log(callback);
    console.error(e);
  }
};

const initializeDB = () => {
  let query = "SELECT * FROM gravja.user";
  let initialized = false;

  executeSQL(query, null, (err, rows) => {
    if (err || !rows) {
      initialized = false;
    } else {
      initialized = true;
    }

    if (!initialized) {
      executeSQL(
        "USE gravja; " +
        "CREATE TABLE gravja.user (" +
        "id INT NOT NULL AUTO_INCREMENT," +
        "username VARCHAR(45) NOT NULL," +
        "password VARCHAR(45) NOT NULL,  PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8;" +
        'INSERT INTO gravja.user (username,password) VALUES("aesthesisM","11231123")',
        null,
        (err, row) => {
          if (err) {
            return console.error(err);
          }
        }
      );
      executeSQL(
        "USE gravja; " +
        "CREATE TABLE gravja.symbol (" +
        "id INT NOT NULL AUTO_INCREMENT," +
        "exchangeName VARCHAR(15) NOT NULL," +
        "quantityIncrement FLOAT," +
        "symbol VARCHAR(10) NOT NULL," +
        "from_ VARCHAR(10) NOT NULL," +
        "to_ VARCHAR(10) NOT NULL," +
        "minimumBTCamount FLOAT," +
        "lastPrice FLOAT," +
        "lastVolume FLOAT,PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;" +
        "ALTER TABLE gravja.symbol ADD UNIQUE (exchangeName,symbol);",
        null,
        (err, rows) => {
          if (err) {
            return console.error(err);
          }
        }
      );
      executeSQL(
        "USE gravja; " +
        "CREATE TABLE gravja.exchange_key (" +
        "id INT NOT NULL AUTO_INCREMENT," +
        "exchangeName VARCHAR(15)," +
        "apiKey VARCHAR(64)," +
        "apiSecret VARCHAR(64), PRIMARY KEY(id))ENGINE=InnoDB DEFAULT CHARSET=utf8;" +
        "INSERT INTO gravja.exchange_key(exchangeName) VALUES ('hitbtc');",
        null,
        (err, rows) => {
          if (err) {
            return console.error(err);
          }
        }
      );
      //chain.status 0 = waiting, 1 running 2 stopped 3 cancelled
      executeSQL(
        "USE gravja; " +
        "CREATE TABLE gravja.chain (" +
        "id INT NOT NULL AUTO_INCREMENT," +
        "name VARCHAR(30) NOT NULL," +
        "exchangeName VARCHAR(15) NOT NULL," +
        "status TINYINT NOT NULL DEFAULT 0," +
        "created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
        "end_time DATE DEFAULT NULL," +
        "repeatable TINYINT NOT NULL DEFAULT 0, PRIMARY KEY(id))ENGINE=InnoDB DEFAULT CHARSET=utf8; ",
        null,
        (err, rows) => {
          if (err) {
            return console.error(err);
          }
        }
      );
      executeSQL(
        "USE gravja; " +
        "CREATE TABLE gravja.order (" +
        "id INT NOT NULL AUTO_INCREMENT," +
        "exchangeName VARCHAR(15) NOT NULL," +
        "clientOrderId VARCHAR(40)," +
        "order_ INT DEFAULT 0," +
        "price FLOAT," +
        "amount FLOAT," +
        "buysell VARCHAR(5)," +
        "from_ VARCHAR(10)," +
        "to_ VARCHAR(10)," +
        "market VARCHAR(20)," +
        "status TINYINT DEFAULT 0," +
        "created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
        "completed_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
        "chainId INT(11) NOT NULL," +
        "PRIMARY KEY(id))ENGINE=InnoDB DEFAULT CHARSET=utf8;",
        null,
        (err, rows) => {
          if (err) {
            return console.error(err);
          }
        }
      );

      executeSQL(
        "USE gravja;" +
        "CREATE TABLE gravja.happened_order(" +
        "id INT NOT NULL AUTO_INCREMENT," +
        "orderId INT NOT NULL," +
        "completed_time TIMESTAMP  DEFAULT CURRENT_TIMESTAMP," +
        "PRIMARY KEY(id))ENGINE=InnoDB DEFAULT CHARSET=utf8;",
        null,
        (err, rows) => {
          if (err) {
            return console.error(err);
          }
        }
      );

      executeSQL(
        "USE gravja;" +
        "CREATE TABLE gravja.historical_data (" +
        "id INT NOT NULL AUTO_INCREMENT," +
        "symbol VARCHAR(10) NULL," +
        "max FLOAT NULL," +
        "min FLOAT NULL," +
        "close FLOAT NULL," +
        "open FLOAT NULL," +
        "timestamp TIMESTAMP NULL," +
        "volume FLOAT NULL," +
        "period VARCHAR(10) NULL," +
        "exchangeName VARCHAR(10) NULL," +
        "ma9_division_price_close FLOAT NULL," +
        "ma40_division_price_close FLOAT NULL," +
        "ma90_division_price_close FLOAT NULL," +
        "ma200_division_price_close FLOAT NULL," +
        "PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8;" +
        "ALTER TABLE gravja.historical_data ADD UNIQUE (exchangeName,symbol,timestamp)",
        null,
        (err, rows) => {
          if (err) {
            return console.error(err);
          }
        }
      );

      console.log("DB initialized successfully.");
    } else {
      console.log("DB already initialized");
    }
  });
};

module.exports = {
  executeSQL,
  getConnection,
  closeConnection,
  initializeDB
};

initializeDB();
