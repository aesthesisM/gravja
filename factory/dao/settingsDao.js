const db = require('../db/mysql');

const getAllExchangeKeySet = callback => {
    let query = 'SELECT * FROM gravja.exchange_key';

    db.executeSQL(query, null, (err, rows) => {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
};

const getExchangeKeySet = (exchangeName, callback) => {
    let query = 'SELECT * FROM gravja.exchange_key WHERE exchangeName = ? LIMIT 1';

    db.executeSQL(query, [exchangeName], (err, row) => {
        if (err) {
            callback(err);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};

const updateExchangeKeySet = (exchangeName, apiKey, apiSecret, callback) => {
    let query = 'UPDATE gravja.exchange_key SET apiKey=?,apiSecret=? WHERE exchangeName=?';

    db.executeSQL(query, [apiKey, apiSecret, exchangeName], (err, row) => {
        if (err) {
            callback(err);
        } else {
            callback(null, row);
        }
    });
};

module.exports = {
    getAllExchangeKeySet,
    getExchangeKeySet,
    updateExchangeKeySet
};
