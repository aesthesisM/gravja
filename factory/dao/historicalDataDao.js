const db = require('../db/mysql');

const getLatestSymbolTimestamp = (symbol, exchangeName, callback) => {
    let query = 'SELECT MAX(timestamp) FROM gravja.historical_data WHERE symbol=? AND exchangeName=?';

    db.executeSQL(query, null, (err, row) => {
        if (err) {
            callback(err);
        } else if (row) {
            if (row instanceof Array) {
                callback(null, row[0].timestamp);
            } else {
                callback(null, row.timestamp);
            }
        }
    });
};

const putHistoricalData = data => {

    let query = 'INSERT IGNORE INTO gravja.historical_data (symbol,max,min,close,open,timestamp,volume,period,exchangeName) VALUES (?,?,?,?,?,?,?,?,?)';

    db.executeSQL(
        query,
        [
            data.symbol,
            data.max,
            data.min,
            data.close,
            data.open,
            data.timestamp,
            data.volume,
            data.period,
            data.exchangeName
        ],
        (err, row) => {
            if (err) {
                console.error(err);
            } else {
            }
        }
    );
};

const getSymbolHistoricalData = (exchangeName, symbol, period, callback) => {
    let query = 'SELECT * FROM gravja.historical_data WHERE exchangeName = ? AND symbol= ? and period = ?';

    db.executeSQL(query, [exchangeName, symbol, period], (err, rows) => {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
};

module.exports = {
    getLatestSymbolTimestamp,
    putHistoricalData,
    getSymbolHistoricalData
};
