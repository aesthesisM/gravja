const db = require('../db/mysql');

/*
chain
status 0 = waiting
status 1 = running
status 2 = completed
status 3 = stopped
status 4 = deleted
limit = 20 
each page will be limited to show only 20 chain
*/

/*
order
status 0 = waiting
status 1 = completed
status 2 = stopped
status 3 = deleted
*/
const getChains = (status, exchangeName, pageId, callback) => {
    let limit = 20;
    let rowCount = pageId * limit;

    let query = 'SELECT * FROM gravja.chain WHERE status in (?) AND exchangeName = ? ORDER BY created_time DESC LIMIT ?, ?';

    try {
        db.executeSQL(query, [status, exchangeName, rowCount, limit], (err, rows) => {
            if (err) {
                callback(err);
            } else {
                console.log(rows);
                callback(null, rows);
            }
        });
    } catch (ex) {
        callback(ex);
    }
};

const createChain = (chain, callback) => {
    let query = 'INSERT INTO gravja.chain (name,exchangeName,status,repeatable,end_time) VALUES(?,?,?,?,?)';

    db.executeSQL(query, [chain.name, chain.exchangeName, chain.status, chain.repeatable, chain.end_time], (err, row) => {
        if (err) {
            console.error(err);
            callback(err);
        } else {
            callback(null, row.insertId);
        }
    });
};

const updateChain = (chain, callback) => {
    let query = 'UPDATE gravja.chain SET name=?,status=?,repeatable=?,end_time=? WHERE id = ?';
    db.executeSQL(query, [chain.name, chain.status, chain.repeatable, chain.end_time, chain.id], (err, row) => {
        if (err) {
            callback(err);
        } else {
            callback(null, row);
        }
    });
};

const getChain = (id, callback) => {
    let query = 'SELECT * FROM gravja.chain WHERE id = ?';

    db.executeSQL(query, id, (err, row) => {
        if (err) {
            callback(err);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};
const getChainOrders = (chainId, callback) => {
    let query = 'SELECT * FROM gravja.order WHERE chainId = ? ORDER BY order_ ASC';

    db.executeSQL(query, [chainId], (err, rows) => {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
};

const getChainFirstOrder = (chainId, callback) => {
    //get only waiting first order
    let query = 'SELECT * from gravja.order WHERE chainId = ? AND status = 0 ORDER BY  order_ ASC LIMIT 1';

    db.executeSQL(query, [chainId], (err, row) => {
        if (err) {
            callback(err);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};

const getChainNextOrder = (chainId, order_, callback) => {
    //get only next waiting order
    let query = 'SELECT * FROM gravja.order WHERE chainId = ? AND status = 0  AND order_> ? ORDER BY order_ LIMIT 1';
    db.executeSQL(query, [chainId, order_], (err, row) => {
        if (err) {
            callback(err);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};

const putOrder = (order, callback) => {
    let query = 'INSERT INTO gravja.order (exchangeName,order_,price,amount,buysell,from_,to_,market,chainId) VALUES (?,?,?,?,?,?,?,?,?)';

    db.executeSQL(query, [order.exchangeName, order.order_, order.price, order.amount, order.buysell, order.from_, order.to_, order.market, order.chainId], (err, row) => {
        if (err) {
            callback(err);
        } else {
            callback(null, row);
        }
    });
};

const getOrder = (id, callback) => {
    let query = 'SELECT * FROM gravja.order WHERE id = ?';

    db.executeSQL(query, [id], (err, row) => {
        if (err) {
            callback(null);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};

const updateOrder = (order, callback) => {
    if (order.price < 1) {
        order.price = parseFloat(order.price).toFixed(8);
    }
    let query = 'UPDATE gravja.order SET order_=?,price=?,amount=?,buysell=?,from_=?,to_=?,completed_time=?,status=?,market=?,clientOrderId=? WHERE id=?';

    db.executeSQL(
        query,
        [
            order.order_,
            order.price,
            parseFloat(order.amount).toFixed(8),
            order.buysell,
            order.from_,
            order.to_,
            order.completed_time,
            order.status,
            order.market,
            order.clientOrderId,
            order.id
        ],
        (err, row) => {
            if (err) {
                callback(err);
            } else {
                callback(null, row);
            }
        }
    );
};

const chainStatusUpdate = (chainId, status, callback) => {
    let query = 'UPDATE gravja.chain SET status = ? WHERE id = ?';

    db.executeSQL(query, [status, chainId], (err, data) => {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    });
};

const orderStatusUpdate = (orderId, status, callback) => {
    let query = 'UPDATE gravja.order SET status = ? WHERE id = ?';

    db.executeSQL(query, [status, orderId], (err, data) => {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    });
};

const getOrderedFirstChainOrder = (chainId, callback) => {
    let query = 'SELECT * FROM gravja.order WHERE clientOrderId IS NOT NULL AND status = 0 AND chainId = ? ORDER BY order_ ASC LIMIT 1';

    db.executeSQL(query, [chainId], (err, row) => {
        if (err) {
            callback(err);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};

const getAllOrderedChainOrders = callback => {
    let query = 'SELECT * FROM gravja.order WHERE clientOrderId IS NOT NULL AND status=0';

    db.executeSQL(query, null, (err, rows) => {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
};

const getWaitingRecursiveOrder = (chainId, orderId, callback) => {
    let query = 'SELECT * FROM gravja.order WHERE chainId = ? and id NOT IN (?) LIMIT 1';

    db.executeSQL(query, [chainId, orderId], (err, row) => {
        if (err) {
            callback(err);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};

const putHappenedOrder = (orderId, callback) => {
    let query = 'INSERT INTO gravja.happened_order (orderId) VALUES (?)';

    db.executeSQL(query, [orderId], (err, row) => {
        if (err) {
            callback(err);
        } else {
            callback(null, row);
        }
    });
};

const getExchangeHappenedOrders = (exchangeName, pageId, callback) => {
    let limit = 20;
    let rowCount = pageId * limit;
    let query = 'SELECT * FROM gravja.order order JOIN gravja.happened_order happened ON order.id = happened.orderId WHERE order.exchangeName =? LIMIT ?,?';

    db.executeSQL(query, [exchangeName, rowCount, limit], (err, rows) => {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
};

const getRunningChains = callback => {
    let query = 'SELECt * from gravja.chain WHERE status = 1';
    db.executeSQL(query, null, (err, rows) => {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
};

module.exports = {
    getChains,
    createChain,
    updateChain,
    getChain,
    getChainOrders,
    getChainFirstOrder,
    getChainNextOrder,
    putOrder,
    getOrder,
    updateOrder,
    chainStatusUpdate,
    orderStatusUpdate,
    getOrderedFirstChainOrder,
    getAllOrderedChainOrders,
    putHappenedOrder,
    getExchangeHappenedOrders,
    getWaitingRecursiveOrder,
    getRunningChains
};
