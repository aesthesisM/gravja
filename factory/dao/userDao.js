const db = require('../db/mysql');

const getUser = (userName, password, callback) => {
    let query = 'SELECT id,username FROM gravja.user WHERE username = ? AND password = ?';
    db.executeSQL(query, [userName, password], (err, row) => {
        if (err || !row) {
            callback(err);
        } else {
            if (Array.isArray(row)) {
                callback(null, row[0]);
            } else {
                callback(null, row);
            }
        }
    });
};

const changePassword = (userId, newPassword, callback) => {
    let query = 'UPDATE gravja.user set password = ? WHERE id = ?';

    db.executeSQL(query, [newPassword, userId], (err, row) => {
        if (err) {
            callback(err);
        } else {
            callback(null, row);
        }
    });
};

module.exports = {
    getUser,
    changePassword
};
//getUser('aesthesisM','11231123');
