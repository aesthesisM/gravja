const db = require('../db/mysql');

const getSymbols = (exchangeName, callback) => {
    let query = 'SELECT symbol,from_,to_,quantityIncrement,lastPrice,lastVolume FROM gravja.symbol WHERE exchangeName = ?';

    db.executeSQL(query, [exchangeName], (err, rows) => {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
};

const putSymbol = (exchangeName, symbol, callback) => {
    let query =
        'INSERT INTO gravja.symbol(exchangeName,quantityIncrement,symbol,minimumBTCamount,from_,to_) VALUES (?,?,?,?,?,?)';

    db.executeSQL(query, [exchangeName, symbol.quantityIncrement, symbol.symbol, symbol.minimumBTCamount, symbol.from_, symbol.to_], err => {
        if (err) {
            callback(err);
        } else {
            symbol.id = this.lastID;
            callback(null);
        }
    });
};

const updateSymbol = (symbol, callback) => {
    let query = 'UPDATE gravja.symbol SET lastPrice = ?, lastVolume = ? WHERE exchangeName = ? and symbol = ?';
    if (isNaN(symbol.lastPrice)) {
        symbol.lastPrice = 0;
    }
    db.executeSQL(query, [symbol.lastPrice, symbol.lastVolume, symbol.exchangeName, symbol.symbol], (err, data) => {
        if (err) {
            callback(err);
        } else {
            callback(null, data);
        }
    });
};

const deleteSymbols = (exchangeName,callback)=>{

    let query = 'DELETE FROM gravja.symbol WHERE exchangeName = ?; commit;';

    db.executeSQL(query,[exchangeName],(err,data)=>{
        if(err){
            callback(err);
        }else{
            callback(null,data);
        }
    });
}

const getHistoricalSymbols = (exchangeName, callback) => {
    let query =
        'SELECT symbol,lastPrice FROM gravja.symbol WHERE TO_= "BTC" and exchangeName = ? and lastVolume >10 UNION SELECT symbol,lastPrice from gravja.symbol where symbol="BTCUSD" and exchangeName = ? order by lastPrice desc';

    db.executeSQL(query, [exchangeName, exchangeName], (err, rows) => {
        if (err) {
            callback(err);
        } else if (rows != null && rows.length > 0) {
            callback(null, rows);
        }
    });
};

module.exports = {
    getSymbols,
    putSymbol,
    updateSymbol,
    getHistoricalSymbols,
    deleteSymbols
};
